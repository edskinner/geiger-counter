# geiger-counter

This is a "work in progress."

Count radiation-detection events (alpha and beta particles, and gamma rays) and indicate the
real-time dosage on an analog meter, and display historical information via node-red to
WiFi-connected devices via a web page.

The system uses a WiFi-enabled Raspberry Pi, radiation detector, and a 0-100 milliamp analog meter.

## Getting Started

You'll need a Raspberry Pi configuration for software development.  I used a Raspberry Pi 4 model B.

### Prerequisites

You will need:
* Raspberry Pi board with WiFi
* Radiation Detector board
* Geiger-Muller tube
* 0-100 milliamp analog meter

### Installing

Install Raspbian on the Raspberry Pi. (I used Raspbian/Debian 10.0 "Buster".) Boot and configure as desired.

Add the basic development tools (as the root user):
* apt-get update
* apt-get upgrade
* apt-get install build-essential make

You may also desire:
* apt-get install manpages-dev glibc-doc

## Deployment

TO BE WRITTEN: Add additional notes about how to deploy this on a live system

## Notes and Comments

WARNING: The radiation detector board I used has up to 500v present at some locations.
DO NOT STICK YOUR HAND IN THERE!

The radiation detector, referred to herein as the detector board, is the Arduino Compatible ver 3.00 unit
from [RH Electronics](http://www.rhelectronics.net/). (FYI: I bought and assembled the kit version.)
This radiation detector did not include the essential Geiger Muller tube.
Accordingly, I purchased an SBM-20 Geiger Muller tube (tested and "known good" by the seller) from a Russian
seller on Ebay. In completing the board, I opted for the capacitive output (C-INT installed, R-INT omitted)
and did my early testing with an Arduino Uno.
The detector board is powered from the 5v and Common pins of either an Arduino or a Raspberry Pi (GPIO header),
P1-06 (Gnd) and P1-04 (5v).

The INT output of the detector board is connected to the Raspberry Pi, GPIO 23, P1-16 via a 10K series resistor.
As measured on an oscilloscope, the detection pulse is 4-8 uSec (microseconds) in duration.
The internal pull-up for that input is enabled (by this software) in the Raspberry Pi.
Additionally, it also looks like the initialization will need to enable Asynchronous Falling Edge Detection via
a call to bcm2835_gpio_afen(pin#). The pin# is coded as RPI_GPIO_P1_nn where "nn" matches the number on the
26-pin header (with a leading 0 for pins 1-9).

NOTE: The detector board uses 5v logic whereas the Raspberry Pi uses 3.3v logic. The C-INT connection isolates
the differing DC levels between the two boards, and the series resistor limits the current in-rush to the
Raspberry Pi. No other voltage adjustment or protection is required.

## Authors

[Ed Skinner](https://github.com/edskinner)

## License

This project is under the GPL License - see [LICENSE.md](LICENSE.md) for details

## Acknowledgments

* [Derek Molloy](https://github.com/derekmolloy)
