/*
 * @file   gpio_intcnt.c
 * Ed Skinner (based on a kernel module by Derek Molloy)
 * @date   2 September 2019
 * @brief  A kernel module for counting interrupts on a GPIO pin.
*/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/gpio.h>                 /* Required for the GPIO functions */
#include <linux/interrupt.h>            /* Required for the IRQ code */

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ed Skinner");
MODULE_DESCRIPTION("Count interrupts on a GPIO pin");
MODULE_VERSION("0.1");

static unsigned int gpioIrqPin = 115;   /* GPIO pin is P9_27 (GPIO115) */
static unsigned int irqNumber;          /* < Used to share the IRQ number within this file */
static unsigned int intCount = 0;  /* < For information, store the number of button presses */

/* Prototype for the custom IRQ handler function */
static irq_handler_t  gpio_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs);

/* @brief The LKM initialization function
 * Connect to the IRQ.
 * @return returns 0 if successful
 */
static int __init gpio_init(void){

   int result = 0;
   printk(KERN_INFO "gpio_intcnt: Initializing the gpio_intcnt LKM\n");
 
   gpio_request(gpioIrqPin, "sysfs");       /* Set up the gpioIrqPin */
   gpio_direction_input(gpioIrqPin);        /* Set the interrupt GPIO pin as input */
   TODO("HOW DO WE TURN OFF DEBOUNCE?");
   gpio_set_debounce(gpioIrqPin, 1);        /* Debounce the button with a delay of 200ms */
   gpio_export(gpioIrqPin, false);          /* Causes gpio115 to appear in /sys/class/gpio */

   /* Perform a quick test to see that the interrupt pin is working as expected on LKM load */
   printk(KERN_INFO "gpio_intcnt: The button state is currently: %d\n", gpio_get_value(gpioIrqPin));

   /* Translate GPIO pin number to IRQ number */
   irqNumber = gpio_to_irq(gpioIrqPin);
   printk(KERN_INFO "gpio_intcnt: The GPIO interrupt pin is mapped to IRQ: %d\n", irqNumber);

   /* Attach to the interrupt */
   result = request_irq(irqNumber,             /* The interrupt number requested */
                        (irq_handler_t) gpio_irq_handler, /* The pointer to the handler function below */
                        IRQF_TRIGGER_FALLING,   /* Interrupt on falling edge */
                        "gpio_intcnt",    /* Used in /proc/interrupts to identify the owner */
                        NULL);                 /* The *dev_id for shared interrupt lines, NULL is okay */

   printk(KERN_INFO "gpio_intcnt: The interrupt request result is: %d\n", result);
   return result;
}

/** @brief The LKM cleanup function
 *  Similar to the initialization function, it is static. The __exit macro notifies that if this
 *  code is used for a built-in driver (not a LKM) that this function is not required. Used to release the
 *  GPIOs and display cleanup messages.
 */
static void __exit gpio_exit(void){
   free_irq(irqNumber, NULL);               /* Free the IRQ number, no *dev_id required in this case */
   gpio_unexport(gpioIrqPin);               /* Unexport the Button GPIO*/
   gpio_free(gpioIrqPin);                   /* Free the Button GPIO*/
   printk(KERN_INFO "gpio_intcnt: Goodbye from the LKM!\n");
}

/** @brief The GPIO IRQ Handler function
 *  This function is a custom interrupt handler that is attached to the GPIO pin we wish to count.
 *  @param irq    the IRQ number associated with the GPIO pin
 *  @param dev_id the *dev_id that is provided
 *  @param regs   h/w registers at the time of the interrupt (not interesting)
 *  return returns IRQ_HANDLED if successful -- should return IRQ_NONE otherwise.
 */
static irq_handler_t gpio_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs){
   TODO("Remove the printk in the ISR -- it'll become excessive in \"hot\" environments");
   printk(KERN_INFO "gpio_intcnt: Interrupt! (button state is %d)\n", gpio_get_value(gpioIrqPin));
   intCount++;                         /* Count the interrupts */
   return (irq_handler_t) IRQ_HANDLED;      /* Announce that the IRQ has been handled correctly */
}

/* Identify the initialization and exit functions */
module_init(gpio_init);
module_exit(gpio_exit);
